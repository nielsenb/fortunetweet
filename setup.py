from setuptools import setup, find_packages

setup(name = 'fortunetweet',
      version = '0.43',
      packages = find_packages(),
      zip_safe = True,
      install_requires = [
        'python-twitter',
      ],
)
