#Copyright 2010 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess

from util import loghelper
from decimal import Decimal

"""Temperature reading libray. Takes a temperature
and converts it in to a tweetable phrase on request.

Currently only supports CPU temperature.
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.41'

class Temperature(object):
    def __init__(self, configoptions):
        #Set up logging
        self._logger = loghelper.getlogger(__name__)

        self._cputempadapter = configoptions.get('CPUtemp', 'adapter')
        self._cputempsensor = configoptions.get('CPUtemp', 'sensor')

    def gettwittercputemp(self):
        """Gets a tweet compatible string representing the CPU temperature in
        Celsius.
        """

        #Pick out the CPU temp in Celsius (cast to an int)
        cputemp = Decimal(self._getcputemp())

        cputemp = 'My CPU is currently {temp} degrees Celsius.'.format(temp=cputemp)

        return cputemp

    def _getcputemp(self):
        """Returns the CPU temperature in degrees Celsius.
        """

        return self._parsesensordata(self._cputempadapter, self._cputempsensor)

    def _parsesensordata(self, adapter, sensor):
        """Gets the temperature value for the given adapter
        and sensor.

        Keyword arguments:
        adapter -- the lm_sensors detected adapter to read from
        sensor -- the actual sensor (eg. temp1) to pull data from
        """

        #Use lm_sensors to read sensor data
        sensorscommand = 'sensors'
        sensorsprocess = subprocess.Popen(sensorscommand, stdout = subprocess.PIPE)

        [sensordata, sensorerr] = sensorsprocess.communicate()

        self._logger.info('Sensor process returned the following: {sensordata}, {sensorerr}'.format(sensordata=sensordata, sensorerr=sensorerr))

        #Remove anything before the adapter we're interested in
        sensordata = sensordata
        sensordata = sensordata.split(adapter)[1]

        #Remove anything before the sensor we're interested in
        sensordata = sensordata.split(sensor + ':')[1].strip()

        #Pull the temp data
        #NOTE: Optimally, we would split on the 'degree' symbol, but that
        #      gets us in to a character encoding mess.
        #      To get around this, for now we split on the decimal, no
        #      sensor we have seen uses the precision anyway.
        sensordata = sensordata.split('.')[0]

        return sensordata
