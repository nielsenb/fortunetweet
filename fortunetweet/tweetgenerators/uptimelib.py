#Copyright 2010 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import random
import shlex

from util import loghelper
from util.constants import secondsperhour
from decimal import Decimal

"""Library for taking various uptime and load average
metrics and converts them in to a tweetable phrase.
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.41'

class Uptime(object):
    def __init__(self, configoptions):
        #Set up logging
        self._logger = loghelper.getlogger(__name__)

        self._uptimecommand = configoptions.get('Uptime', 'command')
        self._idlecommand = configoptions.get('Idletime', 'command')
        self._loadavgcommand = configoptions.get('Loadavg', 'command')
        self._cpucommand = configoptions.get('Loadavg', 'cpucommand')

    def gettwitteruptime(self):
        """Returns a tweet containing uptime in hours.
        """

        uptimecommand = shlex.split(self._uptimecommand)
        uptimeprocess = subprocess.Popen(uptimecommand, stdout = subprocess.PIPE)

        [uptime, uptimeerr] = uptimeprocess.communicate()

        self._logger.info('Uptime process returned the following: {uptime}, {uptimeerr}'.format(uptime=uptime, uptimeerr=uptimeerr))

        #Get the uptime in seconds
        uptime = Decimal(uptime.split()[0])

        #Convert uptime to hours
        uptime = uptime / secondsperhour()

        uptime = 'I have been up for %(time)d hours.' % {'time': uptime}

        return uptime

    def gettwitteridletime(self):
        """Returns a tweet containing total idle time in hours.
        """

        idletimecommand = shlex.split(self._idlecommand)
        idletimeprocess = subprocess.Popen(idletimecommand, stdout = subprocess.PIPE)

        [idletime, idletimeerr] = idletimeprocess.communicate()

        self._logger.info('Idle time process returned the following: {idletime}, {idletimeerr}'.format(idletime=idletime, idletimeerr=idletimeerr))

        #Get the idle time in seconds
        idletime = Decimal(idletime.split()[1])

        #Convert idle time to hours, note, idle time is a sum over all processors
        idletime = (idletime / secondsperhour()) / self._getcpucount()

        idletime = 'I have been idle for %(time)d hours. Booorrriiinnnggg.' % {'time': idletime}

        return idletime

    def gettwitterrandomloadavg(self):
        """Returns a tweet containing random current, five minute,
        or fifteen minute load average.
        """

        tweettypedecision = random.randint(0, 2)

        avgtypes = {
            0: self.gettwittercurrentloadavg(),
            1: self.gettwitterfiveminuteloadavg(),
            2: self.gettwitterfifteenminuteloadavg(),
        }

        loadavg = avgtypes.get(tweettypedecision)

        return loadavg

    def gettwittercurrentloadavg(self):
        """Returns a tweet containing the current load average.
        """

        loadavg = 'My current load average is %(loadavg).2f of a possible %(cpu).2f.' % {'loadavg': self._getcurrentloadavg(), 'cpu': self._getcpucount()}

        return loadavg

    def gettwitterfiveminuteloadavg(self):
        """Returns a tweet containing the five minute load average.
        """

        loadavg = 'My load average for the last five minutes is %(loadavg).2f of a possible %(cpu).2f.' % {'loadavg': self._getfiveminuteloadavg(), 'cpu': self._getcpucount()}

        return loadavg

    def gettwitterfifteenminuteloadavg(self):
        """Returns a tweet containing the fiteen minute load average.
        """

        loadavg = 'My load average for the last fifteen minutes is %(loadavg).2f of a possible %(cpu).2f.' % {'loadavg': self._getfifteenminuteloadavg(), 'cpu': self._getcpucount()}

        return loadavg

    def _getcurrentloadavg(self):
        """Returns the decimal current load average.
        """

        loadavgcommand = shlex.split(self._loadavgcommand)
        loadavgprocess = subprocess.Popen(loadavgcommand, stdout = subprocess.PIPE)

        [loadavg, loadavgerr] = loadavgprocess.communicate()

        self._logger.info('Load average process returned the following: {loadavg}, {loadavgerr}'.format(loadavg=loadavg, loadavgerr=loadavgerr))

        #Get the load average for the last minute
        loadavg = Decimal(loadavg.split()[0])

        return loadavg

    def _getfiveminuteloadavg(self):
        """Returns the decimal five minute load average.
        """

        loadavgcommand = shlex.split(self._loadavgcommand)
        loadavgprocess = subprocess.Popen(loadavgcommand, stdout = subprocess.PIPE)

        [loadavg, loadavgerr] = loadavgprocess.communicate()

        self._logger.info('Load average process returned the following: {loadavg}, {loadavgerr}'.format(loadavg=loadavg, loadavgerr=loadavgerr))

        #Get the load average for the last five minutes
        loadavg = Decimal(loadavg.split()[1])

        return loadavg

    def _getfifteenminuteloadavg(self):
        """Returns the decimal fiteen minute load average.
        """

        loadavgcommand = shlex.split(self._loadavgcommand)
        loadavgprocess = subprocess.Popen(loadavgcommand, stdout = subprocess.PIPE)

        [loadavg, loadavgerr] = loadavgprocess.communicate()

        self._logger.info('Load average process returned the following: {loadavg}, {loadavgerr}'.format(loadavg=loadavg, loadavgerr=loadavgerr))

        #Get the load average for the last fifteen minutes
        loadavg = Decimal(loadavg.split()[2])

        return loadavg

    def _getcpucount(self):
        """Returns the number of CPUs.
        """

        #Get the number of cores
        numcpucommand = shlex.split(self._cpucommand)
        numcpuprocess = subprocess.Popen(numcpucommand, stdout = subprocess.PIPE)

        [numcpu, numcpuerr] = numcpuprocess.communicate()

        self._logger.info('CPU count process returned the following: {numcpu}, {numcpuerr}'.format(numcpu=numcpu, numcpuerr=numcpuerr))

        numcpu = Decimal(numcpu.split()[3])

        return numcpu
