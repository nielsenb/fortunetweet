#Copyright 2010 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import shlex

from util import loghelper

"""Basic library for getting twitter
compatible strings from fortune.
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.3'

class Fortune(object):
    def __init__(self, configoptions):
        #Set up logging
        self._logger = loghelper.getlogger(__name__)

        self._fortunecommand = configoptions.get('Fortune', 'command')

    def gettwitterfortune(self):
        """Returns a twitter compatible string
        from fortune.
        """

        #Convert the command in the config file to something we can use
        fortunecommand = shlex.split(self._fortunecommand)

        fortuneprocess = subprocess.Popen(fortunecommand, stdout = subprocess.PIPE)

        [fortune, fortuneerr] = fortuneprocess.communicate()

        self._logger.info('Fortune process returned the following: {fortune}, {fortuneerr}'.format(fortune=fortune, fortuneerr=fortuneerr))

        #Strip any newlines
        fortune = fortune.strip('\n')

        return fortune
