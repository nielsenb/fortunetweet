#Copyright 2012 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

def secondsperminute():
    """Returns the number of seconds in a minute.
    """

    return 60

def secondsperhour():
    """Returns the number of seconds in an hour.
    """

    return secondsperminute() * minutesperhour()

def minutesperhour():
    """Returns the number of minutes in an hour.
    """

    return 60
