import logging

def getlogger(name):
    """Instantiates and returns a properly configured
    logger, ensuring multiple loggers will not be attached.

    Keyword arguments:
    name -- the name of the module requesting a logger
    """

    #Set up logging
    logger = logging.getLogger(name)

    #Do not instantiate multiple loggers
    if len(logger.handlers) == 0:
        logger.setLevel(logging.INFO)
        consolehandler = logging.StreamHandler()
        loggingformatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
        consolehandler.setFormatter(loggingformatter)
        logger.addHandler(consolehandler)

    return logger
