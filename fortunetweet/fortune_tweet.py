#!/usr/bin/env python

#Copyright 2010 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import signal

from util import loghelper
from twitterlib import connector
from twitterlib.randomtweeter import RandomTweeter
from twitterlib.replier import Replier
from multiprocessing import Process
from ConfigParser import ConfigParser
from optparse import OptionParser

"""Basic library for tweeting your fortune. Also
allows for tweeting of basic computer health statistics,
such as load averages, CPU temp, and uptime.

There is rudimentary support for responding to questions
such as 'what is your uptime?'
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.44'

def main():
    """Executes fortune_tweet, responds to replies, and
    picks something at random to tweet about.

    Keyword arguments:
    argv -- passed in arguments
    """

    config = readconfigurationoptions()

    #Set up logging
    logger = loghelper.getlogger(__name__)

    #Handle <Ctrl>-<C>
    signal.signal(signal.SIGINT, sig_handler)

    #Open the API
    logger.info('Instantiating Twitter API...')
    twitterapi = connector.API()

    #Start the replier if needed
    replyprocess = None
    if config.getboolean('Reply', 'issuereplies') == True or config.getboolean('Run', 'forcereply') == True:
        logger.info('Instantiating replier...')
        replier = Replier(config, twitterapi)

        #Process any replies that need processing
        logger.info('Starting replier process...')
        replyprocess = Process(target=replier.processreplies)
        replyprocess.daemon = True #Ensure the process will be killed if the parent is killed
        replyprocess.start()
        logger.info('Replier process started.')

    logger.info('Instantiating random tweeter...')
    randomtweeter = RandomTweeter(config, twitterapi)

    logger.info('Starting random tweeter process...')
    tweeterprocess = Process(target=randomtweeter.randomtweet)
    tweeterprocess.daemon = True
    tweeterprocess.start()
    logger.info('Random tweeter process started.')

    if config.getboolean('Run', 'reschedule'):
        signal.pause()

    #Don't leave a started reply process hanging
    if replyprocess != None:
        replyprocess.join()

    tweeterprocess.join()

def sig_handler(signal, frame):
    #Handle the program ending
    logger = loghelper.getlogger(__name__)

    logger.info('<Ctrl>-<C> detected, terminating.')

def readconfigurationoptions():
    configoptions = ConfigParser()
    configoptions.readfp(open('defaults.cfg')) #Read the defaults
    configoptions.read('fortunetweet.cfg')

    #Handle any passed in arguments
    parser = OptionParser()

    parser.add_option('-d', '--debug',
                        action='store_true', dest='debugmode', default=False,
                        help='Whether to run in debug mode (do not post tweets). [default: %default]')

    parser.add_option('-r', '--resched',
                        action='store_true', dest='reschedule', default=False,
                        help='Whether to perpetually reschedule tweets. [default: %default]')

    parser.add_option('--fortune',
                        action='store_true', dest='forcefortune', default=False,
                        help='Forces generation of a fortune. [default: %default]')

    parser.add_option('--uptime',
                        action='store_true', dest='forceuptime', default=False,
                        help='Forces generation of a uptime message. [default: %default]')

    parser.add_option('--idletime',
                        action='store_true', dest='forceidletime', default=False,
                        help='Forces generation of a idle time message. [default: %default]')

    parser.add_option('--loadavg',
                        action='store_true', dest='forceloadavg', default=False,
                        help='Forces generation of a load average message. [default: %default]')

    parser.add_option('--cputemp',
                        action='store_true', dest='forcecputemp', default=False,
                        help='Forces generation of a CPU temp message. [default: %default]')

    parser.add_option('--reply',
                        action='store_true', dest='forcereply', default=False,
                        help='Forces generation of a reply. [default: %default]')

    options, args = parser.parse_args()

    #Convert command line arguments to a dictionary
    options = vars(options)

    #Add the command line options to the ConfigParser instance
    for option in options:
        configoptions.set('Run', option, str(options[option]))

    return configoptions

if __name__ == "__main__":
    main()
