#Copyright 2010 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sched
import time

import os.path

from util import loghelper
from util.constants import secondsperminute
from urllib2 import URLError
from tweetgenerators.uptimelib import Uptime
from tweetgenerators.temperaturelib import Temperature

"""Handles basic reply functionality over twitter.
Attempts to answer questions related to 'load status,'
'cpu temp,' and 'uptime.' If a reply contains any of
those words, a tweet will be made accordingly.
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.44'

class Replier(object):
    def __init__(self, config, twitterapi):
        """Instantiate class that handles replies.

        Keyword arguments:
        config -- configuration options
        twitterapi -- authenticated twitterlib instance
        """

        #Set up logging
        self._logger = loghelper.getlogger(__name__)

        self._config = config
        self._twitterapi = twitterapi

        self._uptimelib = Uptime(config)
        self._temperaturelib = Temperature(config)

    def processreplies(self, scheduler=None):
        """Processes all replies since the last time this method
        has been run.
        """

        if self._config.getboolean('Run', 'reschedule'):
            if scheduler is None:
                scheduler = sched.scheduler(time.time, time.sleep)
                scheduler.enter(0, 1, self.processreplies, ([scheduler]))
                scheduler.run()

            nextcheckin = self._config.getint('Reply', 'checkinterval') * secondsperminute()

            scheduler.enter(nextcheckin, 1, self.processreplies, ([scheduler]))
            self._logger.info('Will next check for replies in {nextcheckin:d} seconds.'.format(nextcheckin=nextcheckin))

        try:
            self._logger.info('Checking for replies...')
            messages = self._twitterapi.GetReplies(since_id = self._getlastreplyid())
        except URLError:
            #Whoops, fail whale
            self._logger.error('Twitter is currently unavailable.')
            messages = None

        if messages != None:
            for message in messages:
                tweet = self._processreply(message)

                #Don't print blank replies
                if tweet != None:
                    if self._config.getboolean('Run', 'debugmode') == False:
                        try:
                            self._logger.info('Posting reply...')
                            status = self._twitterapi.PostUpdate(status = tweet)
                            time.sleep(self._config.getint('Reply', 'replydelay') * secondsperminute())
                        except URLError:
                            self._logger.error('Twitter is currently unavailable.')

                    self._logger.info(tweet)

    def _processreply(self, reply):
        """Determines and returns the proper type of reply

        Keyword arguments:
        reply -- the status to reply to
        """

        #Remove the user name
        replytext = reply.text
        replytext = replytext.strip().lower()

        self._logger.info('Replying to the following tweet: {tweet}'.format(tweet=replytext))

        tweet = '@' + reply.user.screen_name + ' '

        self._logger.info('Tweet from the following user: {user}'.format(user=reply.user.screen_name))

        if replytext.find('load status') != -1 and self._config.getboolean('Loadavg', 'enabled'):
            self._logger.info('Replying with load average.')
            tweet += self._uptimelib.gettwittercurrentloadavg()
        elif replytext.find('cpu temp') != -1 and self._config.getboolean('CPUtemp', 'enabled'):
            self._logger.info('Replying with CPU temperature.')
            tweet += self._temperaturelib.gettwittercputemp()
        elif replytext.find('uptime') != -1 and self._config.getboolean('Uptime', 'enabled'):
            self._logger.info('Replying with uptime.')
            tweet += self._uptimelib.gettwitteruptime()
        else:
            self._logger.warning('No suitable reply type found.')
            tweet = None

        #Save the id of the last reply replied to (not done in debug mode)
        if self._config.getboolean('Run', 'debugmode') == False:
            self._logger.info('Setting last reply id to {replyid}.'.format(replyid=reply.id))
            self._setlastreplyid(reply.id)

        return tweet

    def _getlastreplyid(self):
        """Returns the last reply replied to. Returns
        0 if no replies have been made. (Note: the first tweet
        ever had an ID of 0, so this should be fine)
        """

        if os.path.exists('.lastreply') == False:
            self._setlastreplyid(0)

        lastreplyfile = open('.lastreply', 'r')
        lastreplyid = int(lastreplyfile.readline())
        lastreplyfile.close()

        return lastreplyid

    def _setlastreplyid(self, lastreplyid):
        """Saves the passed in ID as the last status
        replied to.

        Keyword arguments:
        lastreplyid -- the ID of the last status replied to
        """

        lastreplyfile = open('.lastreply', 'w')
        lastreplyfile.write(str(lastreplyid))
        lastreplyfile.close()
