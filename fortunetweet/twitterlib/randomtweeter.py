#Copyright 2012 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sched
import time
import random

from util import loghelper
from util.constants import secondsperhour
from urllib2 import URLError
from tweetgenerators.fortunelib import Fortune
from tweetgenerators.uptimelib import Uptime
from tweetgenerators.temperaturelib import Temperature

"""Handles random tweeting.
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.10'

class RandomTweeter(object):
    def __init__(self, config, twitterapi):
        #Set up logging
        self._logger = loghelper.getlogger(__name__)

        self._config = config
        self._twitterapi = twitterapi

        #Create instances of the various tweet generating objects
        self._fortunelib = Fortune(config)
        self._uptimelib = Uptime(config)
        self._temperaturelib = Temperature(config)

        self._mintweettime = config.getint('Reschedule', 'mintweettime')
        self._maxtweettime = config.getint('Reschedule', 'maxtweettime')

    def randomtweet(self, scheduler=None):
        """Will handle generating random tweets (unless tweettype forced)
        and posting them.

        Keyword arguments:
        config -- configuration options
        twitterapi -- authenticated twitterlib instance
        """

        #Schedule another tweet if necessary
        if self._config.getboolean('Run', 'reschedule'):
            if scheduler is None:
                scheduler = sched.scheduler(time.time, time.sleep)
                scheduler.enter(0, 1, self.randomtweet, ([scheduler]))
                scheduler.run()
        
            nexttweetin = random.randint(self._mintweettime, self._maxtweettime) * secondsperhour()

            scheduler.enter(nexttweetin, 1, self.randomtweet, ([scheduler]))
            self._logger.info('Will next tweet in {nexttweet:d} seconds.'.format(nexttweet=nexttweetin))
            
        thistweettype = self._determinetweettype()

        if thistweettype == 'fortune':
            thistweet = self._fortunelib.gettwitterfortune()
        elif thistweettype == 'uptime':
            thistweet = self._uptimelib.gettwitteruptime()
        elif thistweettype == 'idletime':
            thistweet = self._uptimelib.gettwitteridletime()
        elif thistweettype == 'loadavg':
            thistweet = self._uptimelib.gettwitterrandomloadavg()
        elif thistweettype == 'cputemp':
            thistweet = self._temperaturelib.gettwittercputemp()
        else:
            thistweet = 'There was an error generating a valid tweet. :('

        #Post the tweet if there is a tweet to post
        if thistweettype != None:
            if self._config.getboolean('Run', 'debugmode') == False:
                try:
                    self._logger.info('Posting tweet...')
                    status = self._twitterapi.PostUpdate(status = thistweet)
                except URLError:
                    #Whoops, fail whale
                    self._logger.error('Twitter is currently unavailable.')

            self._logger.info(thistweet)

    def _determinetweettype(self):
        #First, check to see if a tweet type is forced
        if self._config.getboolean('Run', 'forcefortune') == True:
            self._logger.info('Forcing fortune tweet.')
            return 'fortune'
        elif self._config.getboolean('Run', 'forceuptime') == True:
            self._logger.info('Forcing uptime tweet.')
            return 'uptime'
        elif self._config.getboolean('Run', 'forceidletime') == True:
            self._logger.info('Forcing idle time tweet.')
            return 'idletime'
        elif self._config.getboolean('Run', 'forceloadavg') == True:
            self._logger.info('Forcing load average tweet.')
            return 'loadavg'
        elif self._config.getboolean('Run', 'forcecputemp') == True:
            self._logger.info('Forcing CPU temperature tweet.')
            return 'cputemp'

        #Set up the matrix of pontential tweet types
        tweetweights = {'fortune': self._config.getint('Fortune', 'weight') * self._config.getboolean('Fortune', 'enabled'), 'uptime': self._config.getint('Uptime', 'weight') * self._config.getboolean('Uptime', 'enabled'), 'idletime': self._config.getint('Idletime', 'weight') * self._config.getboolean('Idletime', 'enabled'), 'loadavg': self._config.getint('Loadavg', 'weight') * self._config.getboolean('Loadavg', 'enabled'), 'cputemp': self._config.getint('CPUtemp', 'weight') * self._config.getboolean('CPUtemp', 'enabled')}

        self._logger.info('Determining next random tweet with the following weights: {weightdict}'.format(weightdict=tweetweights))

        totaltweetweight = 0
        for tweettype, tweetweight in tweetweights.iteritems():
            totaltweetweight += tweetweight

        #If no tweet type enabled, don't tweet
        if totaltweetweight == 0:
            self._logger.warning('No tweet type enabled.')
            return None

        tweettypedecision = random.randint(0, totaltweetweight)

        tweettypeindex = 0
        for tweettype, tweetweight in tweetweights.iteritems():
            tweettypeindex += tweetweight

            if tweettypeindex >= tweettypedecision:
                self._logger.info('{tweettype} selected.'.format(tweettype=tweettype))
                return tweettype
