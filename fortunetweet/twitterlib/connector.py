#Copyright 2010 Brandon Nielsen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pickle
import webbrowser
import twitter
import os.path

import oauth2 as oauth

from util import loghelper
from urlparse import parse_qsl

"""Basic library for authenticating with and interacting with
twitter
"""
__author__ =  'Brandon Nielsen'
__version__ =  '0.40'

class API(twitter.Api):
    def __init__(self):
        """Authenticates against twitter, prompting the
        user for login information if necessary
        """

        #Set up logging
        self._logger = loghelper.getlogger(__name__)

        CONSUMER_KEY, CONSUMER_SECRET = self._loadapikeys()

        #Check for existence of an auth token, if it exists, use it
        if os.path.exists('.authtoken'):
            self._logger.info('Using existing authorization token.')

            authtoken = self._loadauthtoken()
            twitter.Api.__init__(self, CONSUMER_KEY, CONSUMER_SECRET, authtoken['oauth_token'], authtoken['oauth_token_secret'])
        else:
            self._logger.info('No authorization token exists, creating new token using OAuth...')

            REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
            ACCESS_TOKEN_URL  = 'https://api.twitter.com/oauth/access_token'
            AUTHORIZATION_URL = 'https://api.twitter.com/oauth/authorize'
            SIGNIN_URL = 'https://api.twitter.com/oauth/authenticate'

            #No auth token, we need to generate one
            signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()
            oauth_consumer             = oauth.Consumer(key=CONSUMER_KEY, secret=CONSUMER_SECRET)
            oauth_client               = oauth.Client(oauth_consumer)

            resp, content = oauth_client.request(REQUEST_TOKEN_URL, 'GET')

            request_token = dict(parse_qsl(content))

            #Launch a browser so the user can authenticate
            self._logger.info('Opening web browser for user authentication...')
            webbrowser.open('{0}?oauth_token={1}'.format(AUTHORIZATION_URL, request_token['oauth_token']))

            #Wait for user input before continuing
            self._logger.info('Requesting user PIN...')
            pincode = raw_input('Hit <Enter> when you have entered your PIN. <Ctrl-C> cancels.\n')

            token = oauth.Token(request_token['oauth_token'], request_token['oauth_token_secret'])
            token.set_verifier(pincode)

            oauth_client = oauth.Client(oauth_consumer, token)
            resp, content = oauth_client.request(ACCESS_TOKEN_URL, method='POST', body='oauth_verifier=%s' % pincode)
            authtoken = dict(parse_qsl(content))

            self._logger.info('Saving authorization token...')
            self._saveauthtoken(authtoken)

            twitter.Api.__init__(self, CONSUMER_KEY, CONSUMER_SECRET, authtoken['oauth_token'], authtoken['oauth_token_secret'])

    def _saveauthtoken(self, token):
        """Saves the passed in authentication
        token so the user does not need to reauthenticate
        every time the application is run.

        Keyword arguments:
        token -- the authorization token to save
        """

        authfile = open('.authtoken', 'w')

        pickle.dump(token, authfile)

        #Always close files when we're done
        authfile.close()

    def _loadauthtoken(self):
        """Loads an authorization token
        that has been saved.
        """

        authfile = open('.authtoken', 'r')

        authtoken = pickle.load(authfile)

        authfile.close()

        return authtoken

    def _loadapikeys(self):
        """Loads the twitter consumer secret and consumer keys
        """

        apifile = open('.apikeys')

        content = apifile.readlines()

        apifile.close()

        #The consumer key is the first line, consumer secret is the second
        consumer_key = content[0].strip()
        consumer_secret = content[1].strip()

        return consumer_key, consumer_secret
